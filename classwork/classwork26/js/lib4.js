document.addEventListener('DOMContentLoaded', onReady);

const productList = [
    {
        title: 'Парта армейская',
        amount: 10,
        price: 34.56

    },

    {
        title: 'Sleep&Fly Organic Epsilon 160х200 см (2012051602001)',
        amount: 10,
        price: 34.56

    },

    {
        title: 'Come-for Гармонія 160х200 см (2560121602005)',
        amount: 10,
        price: 34.56

    },

    {
        title: 'Матрац LasCavo Альба 160х200 см (4823086611153)',
        amount: 10,
        price: 34.56

    }
];

function onReady() {
    const tables = document.getElementsByClassName('product-list');

    buildList(tables[0]);

    const search = document.getElementById('search');
    search.addEventListener('keypress', function(e) {
        buildList(tables[0], e.target.value);
    })


}

function doFiltration(txt) {
    if (txt) {
        return productList.filter(function(item) {
            return item.title.toLowerCase().indexOf(txt.toLowerCase()) > 0;
        });
    }

    return productList;
}

function buildList(parent, search) {

    for (let child of parent.children) {
        console.log(child.tagName);
    }

    doFiltration(search).forEach(function(product) {
        const trObj = document.createElement('tr');
        const tdTitle = document.createElement('td');

        tdTitle.innerHTML = product.title;

        const tdAmount = document.createElement('td');
        tdAmount.innerHTML = product.amount;

        const tdPrice = document.createElement('td');
        tdPrice.innerHTML = product.price;

        trObj.appendChild(tdTitle);
        trObj.appendChild(tdAmount);
        trObj.appendChild(tdPrice);

        parent.appendChild(trObj);
    });


}

