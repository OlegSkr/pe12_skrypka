document.addEventListener('DOMContentLoaded', onReady);

const productList = [
    {
        title: 'Парта армейская',
        amount: 10,
        price: 34.56

    },

    {
        title: 'Sleep&Fly Organic Epsilon 160х200 см (2012051602001)',
        amount: 10,
        price: 34.56

    },

    {
        title: 'Come-for Гармонія 160х200 см (2560121602005)',
        amount: 10,
        price: 34.56

    },

    {
        title: 'Матрац LasCavo Альба 160х200 см (4823086611153)',
        amount: 10,
        price: 34.56

    }
];

function onReady() {
    const tables = document.getElementsByClassName('product-list');

    buildList(tables[0], '');

    const search = document.getElementById('search');

    search.addEventListener('keyup', function (e) {
        buildList(tables[0], e.target.value);
    });
}

function doFiltration(txt) {
    if (txt) {
        return productList.filter(function (item) {
            return item.title.toLowerCase().indexOf(txt.toLowerCase()) >= 0;
        });
    }

    return productList;
}

/**
 * @desc Функция для построения структуры таблички
 * @param {Object} parent - таблица к которой мы добавляем элементы
 * @param {String} search - значение по которому фильтруются записи
 **/
function buildList(parent, search) {
    const list = doFiltration(search);

    let strTr = list.map(function (item) {
        return `<tr> <td>${item.title}</td> <td> ${item.amount}</td> <td>${item.price}</td></tr>`;
    }).join('');

    parent.innerHTML = '<tr><th>Товар</th> <th>Кол</th><th>Цена</th></tr>';

    if (list.length === 0) {
        parent.innerHTML += '<tr><td colspan="3">Нет записей</td></tr>';
    } else {
        parent.innerHTML += strTr;
    }
}

