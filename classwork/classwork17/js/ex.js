/**
 * функция считает сумму двух чисел
 * @param a
 * @param b
 * @returns {*}
 */

function sum(a, b) {
    return a+b;
}

/**
 * функция умножает две цифры
 * @param a
 * @param b
 * @returns {number}
 */
function mult(a, b) {
    if (a===undefined || b===undefined){
        return 0;
    }
    return a*b;
}

/**
 * подсчитывает количество вхождений в строку
 * @param str
 * @param num
 */
function numInStr(str, num) {
    let counter = 0;

    for (let i = 0; i<str.length; i++){
        let s = str.charAt(i);

        if (s != '' && +s === num) {
            counter++;
        }
    }
    return counter;
}

function diff(a, b) {
    return a-b;
}

function hockey(amountOfHits, onTarget, goals) {
    return goals/amountOfHits*100;
}