## Теоретический вопрос

1. Опишите своими словами как работает цикл forEach.

## Ответ

Цикл forEach являентся одним из методов массива,
который используется для перебора элементов массива:

arr.forEach(callback[, thisArg])

Этод метод имеет два параметра:
1) функция `callback`, которая вызывается для каждого элемента
массива; этой функции метод передает три параметра:
- item - текущий элемент массива
- i - номер элемента массива
- arr - массив, который перебирается

2) второй, необязательный параметр указывает контекст
`this` для `callback`
