//Написать функцию filterBy(), которая будет принимать в себя 2 аргумента.
// Первый аргумент - массив, который будет содержать в себе любые данные,
// второй аргумент - тип данных.
function filterBy(arr,strType) {
    return arr.filter(function(elem) {
        if (strType === 'array') {
            if (Array.isArray(elem)){
            }
            else{
                return typeof(elem) != strType;
            }
        }
        else if (strType === 'object'){
            if (typeof(elem) === 'object'){
                if (Array.isArray(elem)){
                    return elem;
                }
                else if (elem===null){
                    return elem===null;
                }
            }
            else{
                return typeof(elem) != strType;
            }
        }
        else if (strType === 'null'){
            if (typeof(elem) === 'object'){
                //return typeof(elem) != strType;
                return elem !== null;
            }
            else{
                return typeof(elem) != strType;
            }
        }
        else{
            return typeof(elem) != strType;
        }
    });
}

//Функция должна вернуть новый массив, который будет содержать в себе все данные,
// которые были переданы в аргумент, за исключением тех, тип которых был передан
// вторым аргументом. То есть, если передать массив ['hello', 'world', 23, '23', null],
// и вторым аргументом передать 'string', то функция вернет массив [23, null].

let obj = {};

let arr = [];

let arrTest = ['hello',false, 'world', 23, '23', null,undefined,true,obj,arr];

console.log('arrTest',arrTest);

let strType;
let arrFilter;

//тестируем функцию для шести типов данных

strType = 'string';
arrFilter = filterBy(arrTest,strType);
console.log('\nstrType = \''+strType+'\'');
console.log('arrFilter',arrFilter);

strType = 'number';
arrFilter = filterBy(arrTest,strType);
console.log('\nstrType = \''+strType+'\'');
console.log('arrFilter',arrFilter);

strType = 'boolean';
arrFilter = filterBy(arrTest,strType);
console.log('\nstrType = \''+strType+'\'');
console.log('arrFilter',arrFilter);

strType = 'null';
arrFilter = filterBy(arrTest,strType);
console.log('\nstrType = \''+strType+'\'');
console.log('arrFilter',arrFilter);

strType = 'undefined';
arrFilter = filterBy(arrTest,strType);
console.log('\nstrType = \''+strType+'\'');
console.log('arrFilter',arrFilter);

strType = 'object';
arrFilter = filterBy(arrTest,strType);
console.log('\nstrType = \''+strType+'\'');
console.log('arrFilter',arrFilter);

strType = 'array';
arrFilter = filterBy(arrTest,strType);
console.log('\nstrType = \''+strType+'\'');
console.log('arrFilter',arrFilter);
