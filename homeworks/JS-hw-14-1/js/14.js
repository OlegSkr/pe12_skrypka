let oTabs = $('li.tabs-title');
let oContents = $('.tabs-content li');
let oMenu = $('oMenu');


$('#oMenu').on('click', function(event){
    let target = event.target;
    if (target.tagName != 'LI') return;
    let i;
    let oActiveTab = $('li.tabs-title.active')[0];
    let oActiveContent = $('li.tabs-content.active')[0];
    oActiveTab.setAttribute('class','tabs-title');
    target.setAttribute('class','tabs-title active');

    i = -1;
    for (let oTab of oTabs) {
        i++;
        if(oTab.innerText===target.innerText){break}
    }

let oContent = oContents[i];

     if (oActiveContent.innerText != oContent.innerText){
         oContent.setAttribute('class','tabs-content active');
         oActiveContent.removeAttribute('class');
     }

});

