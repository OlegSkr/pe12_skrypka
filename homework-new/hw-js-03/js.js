function getNumber(smg, errMsg) {
    let number = +prompt(smg);
    while (isNaN(number) || !number){
        number = +prompt(errMsg);
    }
    return number;
}
//
// let number = getNumber('enter right number', 'error try again');
// console.log(number);

function getOperation() {
    let operation = prompt('Enter operation');
    let operations = '+-*/'
    while (!operations.includes(operation)) {
        operation = prompt('error, try again');
    }
    return operation
}


function fun() {
    let a = getNumber('enter first number', 'error try again');
    let b = getNumber('enter second number', 'error try again');
    let operation = getOperation();

    switch (operation) {
        case '+':
            return a + b;
        case '-':
            return a - b;

        case '*':
            return a * b;

        case "/":
            return a / b;
    }
}

let result = fun();
alert(result);