function createNewUser() {

    let firstName = prompt('enter firstName');
    let lastName = prompt('enter lastName');

    let newUser = {
        firstName: firstName,
        lastName: lastName,
        getLogin: function(){
            return (newUser.firstName.charAt(0) + this.lastName).toLowerCase();
        }
    };

    return newUser;
}

const user = createNewUser();
const login = user.getLogin();
console.log(login);