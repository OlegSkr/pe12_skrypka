function createNewUser() {

    let firstName = prompt('enter firstName');
    let lastName = prompt('enter lastName');
    let birthday = prompt('enter your birthday', 'dd.mm.yyyy').split('.');
    let w = birthday[2];

    let newUser = {
        firstName: firstName,
        lastName: lastName,
        getLogin: function(){
            return (newUser.firstName.charAt(0) + this.lastName).toLowerCase();

        },
        getAge: function () {

            let monthNow = new Date().getMonth()+1;
            let yearNow = new Date().getFullYear();

            if (birthday[1]<monthNow){
                return (yearNow-birthday[2])

            }else {
                return(yearNow-birthday[2]-1)

            }

        },
        getPassword: function () {
            return this.firstName.charAt(0).toUpperCase() + this.lastName.toLowerCase()+w;
        }
    };

    return newUser;
}

const user = createNewUser();
const login = user.getLogin();
const password = user.getPassword();
const age = user.getAge();
console.log(login);
console.log(password);
console.log(age);
// console.log(user);