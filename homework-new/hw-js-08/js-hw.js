let userNum =document.getElementsByClassName('inputNum')[0];

userNum.addEventListener('focus', ()=>{
   userNum.style.border='3px solid green';
   userNum.style.borderRadius = '3px';
});
userNum.addEventListener('blur', ()=>{
    userNum.style.border='';
    const spanNum = document.createElement('span');
    const butNum = document.createElement('button');
    spanNum.innerText = `Текущая цена: ${userNum.value}`;
    butNum.innerText = 'x';
    userNum.before(spanNum);
    userNum.before(butNum);
    userNum.style.display = 'flex';
    userNum.style.color = 'green';
    butNum.style.width = '50px';
    
    if (userNum.value<=0){
        userNum.style.border = '3px solid red';
        butNum.remove('butNum');
        spanNum.remove('spanNum');
        const spanNum2 = document.createElement('span');
        userNum.after(spanNum2);
        spanNum2.innerText = 'Please enter correct price';
        let funDel = ()=>{
            spanNum2.innerText = '';
        };
       setTimeout(funDel, 2000)
    }




    butNum.addEventListener('click', ()=>{
        butNum.remove('butNum');
        spanNum.remove('spanNum');
        userNum.value='';
    });

});
