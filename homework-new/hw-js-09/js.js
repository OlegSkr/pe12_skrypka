let x = document.getElementsByClassName('tabs')[0];

x.addEventListener('click', function (event) {
    console.dir(event.target);
    let tabs = [...document.getElementsByClassName('tabs-title')];
    tabs.forEach(tab => tab.classList.remove('active'));
    event.target.classList.add('active');

    let textBlocks = [...document.getElementsByClassName('tabs-content-item')];
    textBlocks.forEach(block => {
        block.classList.remove('active');
        if (block.dataset.text === event.target.dataset.text) {
            block.classList.add('active');
        }
    });

})

