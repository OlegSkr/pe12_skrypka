

const arr = ['hello', 'world','test','test2', 'hell', 'kol', 'milk'];

function listOutPut(array) {
    const list = document.createElement('ul');
    const arryListItems = array.map(item =>{
        let listItem = `<li>${item}</li>`;
        return listItem;
    });

    arryListItems.forEach(elem=>{
        list.innerHTML += elem;
    });

    document.querySelector('script').before(list);

}

listOutPut(arr);